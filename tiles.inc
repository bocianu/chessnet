var 
    _Pb_b :array [0..1] of word = ($4140,$6160);
    _Pb_w :array [0..1] of word = ($C3C2,$E3E2);
    _Pw_b :array [0..1] of word = ($4342,$6362);
    _Pw_w :array [0..1] of word = ($C1C0,$E1E0);

    _Rb_b :array [0..1] of word = ($4544,$6564);
    _Rb_w :array [0..1] of word = ($C7C6,$E7E6);
    _Rw_b :array [0..1] of word = ($4746,$6766);
    _Rw_w :array [0..1] of word = ($C5C4,$E5E4);

    _Nb_b :array [0..1] of word = ($4948,$6968);
    _Nb_w :array [0..1] of word = ($CBCA,$EBEA);
    _Nw_b :array [0..1] of word = ($4B4A,$6B6A);
    _Nw_w :array [0..1] of word = ($C9C8,$E9E8);

    _Bb_b :array [0..1] of word = ($4D4C,$6D6C);
    _Bb_w :array [0..1] of word = ($CFCE,$EFEE);
    _Bw_b :array [0..1] of word = ($4F4E,$6F6E);
    _Bw_w :array [0..1] of word = ($CDCC,$EDEC);

    _Qb_b :array [0..1] of word = ($5150,$7170);
    _Qb_w :array [0..1] of word = ($D3D2,$F3F2);
    _Qw_b :array [0..1] of word = ($5352,$7372);
    _Qw_w :array [0..1] of word = ($D1D0,$F1F0);

    _Kb_b :array [0..1] of word = ($5554,$7574);
    _Kb_w :array [0..1] of word = ($D7D6,$F7F6);
    _Kw_b :array [0..1] of word = ($5756,$7776);
    _Kw_w :array [0..1] of word = ($D5D4,$F5F4);

    whites_B: array [0..5] of pointer = (@_Pw_b, @_Nw_b, @_Bw_b, @_Rw_b, @_Qw_b, @_Kw_b);
    whites_W: array [0..5] of pointer = (@_Pw_w, @_Nw_w, @_Bw_w, @_Rw_w, @_Qw_w, @_Kw_w);
    blacks_B: array [0..5] of pointer = (@_Pb_b, @_Nb_b, @_Bb_b, @_Rb_b, @_Qb_b, @_Kb_b);
    blacks_W: array [0..5] of pointer = (@_Pb_w, @_Nb_w, @_Bb_w, @_Rb_w, @_Qb_w, @_Kb_w);

    pieces: array [0..3] of pointer = ( @blacks_b, @blacks_w, @whites_B, @whites_w );